#!/bin/bash

source common.sh

TEST_CASE="M0054-i2c"
echo_test_case $TEST_CASE
result=0

paths=(/dev/i2c-0 /dev/i2c-1 /dev/i2c-2 /dev/i2c-3 /dev/i2c-4 /dev/i2c-5)

for path in "${paths[@]}"; do
	if [ ! -e "$path" ]; then
		echo_error "device path missing: $path"
		result=1
	fi
done

if [ $result -eq 0 ]; then
	echo_pass $TEST_CASE
	exit 0
else
	echo_fail $TEST_CASE
	exit 1
fi
