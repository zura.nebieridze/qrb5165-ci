#!/bin/bash

source common.sh

TEST_CASE="M0054-2-cameras"
echo_test_case $TEST_CASE
result=0

vfe_paths=( /
	/sys/bus/platform/drivers/cam_vfe/acb4000.qcom,ife0 /
	/sys/bus/platform/drivers/cam_vfe/acc3000.qcom,ife1 /
	/sys/bus/platform/drivers/cam_vfe/acd9000.qcom,ife-lite0 /
	/sys/bus/platform/drivers/cam_vfe/acdb200.qcom,ife-lite1 /
	/sys/bus/platform/drivers/cam_vfe/acdd400.qcom,ife-lite2 /
	/sys/bus/platform/drivers/cam_vfe/acdf600.qcom,ife-lite3 /
	/sys/bus/platform/drivers/cam_vfe/ace1800.qcom,ife-lite4 /
	)

for path in "${vfe_paths[@]}"; do
	if [ ! -e "$path" ]; then
		echo_error "vfe path missing: $path"
		result=1
	fi
done

if [ $result -eq 0 ]; then
	echo_pass $TEST_CASE
	exit 0
else
	echo_fail $TEST_CASE
	exit 1
fi
