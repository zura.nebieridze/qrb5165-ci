#!/bin/bash

source common.sh

TEST_CASE="M0054-uart"
echo_test_case $TEST_CASE
result=0

paths=(/dev/ttyHS0 /dev/ttyHS1 /dev/ttyHS2 /dev/ttyHS3)

for path in "${paths[@]}"; do
	if [ ! -e "$path" ]; then
		echo_error "device path misssing: $path"
		result=1
	fi
done

if [ $result -eq 0 ]; then
	echo_pass $TEST_CASE
	exit 0
else
	echo_fail $TEST_CASE
	exit 1
fi

