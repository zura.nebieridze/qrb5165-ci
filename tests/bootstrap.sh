set +e

platform=""
if [ $# -eq 0 ]; then
    platform=$("./platform-detect.sh")
    echo "[INFO] detected platform: $platform"
else
    platform="$1"
fi

case "$platform" in
    "M0054-1")
        ./M0054/M0054-1.sh
        ;;
    "M0054-2")
        ./M0054/M0054-2.sh
        ;;
    "M0104-1")
        ./M0104/M0104-1.sh
        ;;
    "M0104-2")
        ./M0104/M0104-2.sh
        ;;
    *)
        echo "[ERROR] Invalid option"
        exit 1
        ;;
esac

exit 0
