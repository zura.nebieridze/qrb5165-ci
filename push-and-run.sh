#!/bin/bash
echo "[INFO] waiting for adb..."
adb wait-for-device

echo "[INFO] removing old tests"
adb shell "rm -rf /home/tests"
echo "[INFO] copying new tests"
adb push tests /home/
adb shell "cd /home/tests && ./bootstrap.sh"