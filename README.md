# qrb5165-ci

## Summary

Project used to setup and run tests ON TARGET executed by a gitlab runner bash shell executor.

## Required

- 865 platform with internet access

## First Time Setup

### Setup new Runner

Install gitlab runner on 865 platform.

```
adb shell
git clone https://gitlab.com/voxl-public/system-image-build/qrb5165-ci.git
cd qrb5165-ci/setup
./install-runner.sh
```

## Prepare to Register

Prepare the following information for this runner:

`Description` - Name of the runner you are registering, use format like: `TF-M0054-1-A`

`Tag`: Tag(s) the runner will be able to run. These determine what kind of jobs the runner should execute. (Ex: M0054)

## Create Runner in Gitlab

On a PC, open browser and go to your gitlab instance.  Create the runner in the gitlab project, and obtain the following for the next step:

`Gitlab instance URL`: If you are on gitlab public, the instance URL is https://www.gitlab.com

`Authorization Token`: This is a unique token that can be found in gitlab by navigating to the repository that the CI runner will live in → Settings → CI/CD → Runners → Project runners.

## Register Runner

Back on 865:

```
./register-runner.sh [Description] [Tags] [Gitlab Instance URL] [Registration Token]
```

# Tests

## Context

Everything is relative to the starting location of 'tests'.

## common.sh

Put reusable stuff here and source it to use it in files.